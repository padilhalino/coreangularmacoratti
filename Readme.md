Play list no canal Macoratti:
https://www.youtube.com/watch?v=I_H7isJs1Mo&list=PLfrCMox2vsQhlPZvBGAMwn33M9lhhipHj

Adicionado diretamente ao csproj:
  <ItemGroup>
    <DotNetCliToolReference Include="Microsoft.VisualStudio.Web.CodeGeneration.Tools" Version="2.0.4" />
  </ItemGroup>
  
Após criar o DB, executar o seguinte comando no Package Manager Console:
  Scaffold-DbContext "Data Source=LAPTOP-7D6FTQCI\SQLEXPRESS;Initial Catalog=Estudo;Integrated Security=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
Com isso cria o EstudoContext.cs e os Models das tabelas do banco automaticamente.


# Angular CLI (opcional)
Pra usar o Angular CLI no projeto, pra usar o "ng generate" por exemplo, precisa:
* instalar o Angular CLI: nmp i -g @angular/cli@lastest
* criar nova app: ng new teste
* copiar angular-cli.json para a pasta do projeto (CoreAngular)
* editar o angular-cli.json e trocar o "src" de "root" para "ClientApp"
* instalar o Angular CLI como dependência do projeto (cd path_proj): nmp i -g @angular/cli@lastest --save-dev
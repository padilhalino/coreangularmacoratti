import { Component, OnInit } from '@angular/core';
import { ClienteService } from "../../Services/cliente.service"
import { ICliente } from "../../Models/cliente.interface";
import { FormGroup, FormControl, FormBuilder, Validators, Form } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'
})
export class ClienteComponent implements OnInit {

  clientes: ICliente[] = [];
  cliente: ICliente = <ICliente>{};

  // formulário
  formLabel: string;
  isEditMode: boolean = false;
  form: FormGroup | null | undefined;

  constructor(private clienteService: ClienteService, private fb: FormBuilder) {
    this.form = fb.group({
      "nome": ["", Validators.required],
      "endereco": ["", Validators.required],
      "cidade": ["", Validators.required],
      "email": ["", Validators.required]
    });

    this.formLabel = "Adicionar Cliente";
  }

  private getClientes() {
    this.clienteService.getClientes().subscribe(
      data => this.clientes = data,
      error => alert(error),
      () => console.log(this.clientes)
    );
  }

  ngOnInit(): void {
    this.getClientes();
  }

  onSubmit() {
    this.cliente.nome = this.form!.controls["nome"].value;
    this.cliente.endereco = this.form!.controls["endereco"].value;
    this.cliente.cidade = this.form!.controls["cidade"].value;
    this.cliente.email = this.form!.controls["email"].value;
    if (this.isEditMode) {
      this.clienteService.editCliente(this.cliente).subscribe(
        response => {
          this.getClientes();
          this.form!.reset();
        });
    }
    else {
      this.clienteService.addCliente(this.cliente).subscribe(
        response => { this.getClientes(); this.form!.reset(); });
    }
  }

  cancel() {
    this.formLabel = "Adicionar Cliente";
    this.isEditMode = false;
    this.cliente = <ICliente>{};
    this.form!.get("nome")!.setValue("");
    this.form!.get("endereco")!.setValue("");
    this.form!.get("cidade")!.setValue("");
    this.form!.get("email")!.setValue("");
  }

  edit(cliente: ICliente) {
    this.formLabel = "Editar Cliente";
    this.isEditMode = true;
    this.cliente = cliente;
    this.form!.get("nome")!.setValue(cliente.nome);
    this.form!.get("endereco")!.setValue(cliente.endereco);
    this.form!.get("cidade")!.setValue(cliente.cidade);
    this.form!.get("email")!.setValue(cliente.email);
  }

  delete(cliente: ICliente) {
    if (confirm("Deseja excluir o cliente" + cliente.nome + "?")) {
      this.clienteService.deleteCliente(cliente.id).subscribe(
        response => {
          this.getClientes();
          this.cancel();
          this.form!.reset();
        });
    }
  }
}

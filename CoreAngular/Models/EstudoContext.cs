﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CoreAngular.Models
{
  public partial class EstudoContext : DbContext
  {
    public EstudoContext()
    {
    }

    public EstudoContext(DbContextOptions<EstudoContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Clientes> Clientes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        //TODO: #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //optionsBuilder.UseSqlServer("Data Source=LAPTOP-7D6FTQCI\\SQLEXPRESS;Initial Catalog=Estudo;Integrated Security=True;");
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

      modelBuilder.Entity<Clientes>(entity =>
      {
        entity.Property(e => e.Cidade)
                  .HasMaxLength(100)
                  .IsUnicode(false);

        entity.Property(e => e.Email)
                  .HasMaxLength(100)
                  .IsUnicode(false);

        entity.Property(e => e.Endereco)
                  .HasMaxLength(100)
                  .IsUnicode(false);

        entity.Property(e => e.Nome)
                  .HasMaxLength(100)
                  .IsUnicode(false);
      });
    }
  }
}
